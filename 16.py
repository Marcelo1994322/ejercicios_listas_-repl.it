# Autor_ Marcelo Merchan
# Email__angel.merchan@unl.edu.ec

# Given an integer n, create a two-dimensional array of size n×n according to the following rules and print it
# Read an integer:
# Print a value:
# print(a)
#for

y = int(input())
primero = [i for i in range(y)]
out = [[]]*y
for j in range(y):
  out[j] = primero[j:0:-1]
  for l in primero[:y-j]:
    out[j].append(l)
  print(*out[j])
