# Autor_ Marcelo Merchan
# Email__angel.merchan@unl.edu.ec

# Given a list of numbers, count a number of pairs of equal elements.
# Any two elements that are equal to each other should be counted exactly once.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

a = input()
my_lis = list(map(int, a.split()))
count= 0
for x in my_lis:
    for n in range (0,len(my_lis)):
      if my_lis[n]==x and n != my_lis.index(x):
          count = count + 1
count = count/2
print(int(count))