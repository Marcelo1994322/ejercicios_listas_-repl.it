# Autor_ Marcelo Merchan
# Email__angel.merchan@unl.edu.ec

# Given a list of non-zero integers, find and print the first adjacent pair of elements that have the same sign.
# If there is no such pair, print 0.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

a = input()
lista_p = list(map(int, a.split()))
r = 1
# To check list_p for the first 2 consecutive values with the same sign.
for r in range(1,len(lista_p)):
  if lista_p[r]*lista_p[r-1]>0:
    print(str(lista_p[r - 1])+' '+str(lista_p[r]))
    break
  elif r == len(lista_p)-1:
    print("0")

