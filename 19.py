# Autor_ Marcelo Merchan
# Email__angel.merchan@unl.edu.ec

# Instruccions__ Given an odd positive integer n, produce a two-dimensional array of size n×n.
# Fill each element with the character "." . Then fill the middle row, the middle
# column and the diagonals with the character "*".  You'll get an image of a snow flake.
# Print the snow flake in n×n rows and columns and separate the characters with a single space.

# Read an integer:
# a = int(input())
# Print a value:
# print(a)

a = int(input())
b = [["."]*a for i in range(a)]
for y in range(a):
    for x in range(a):
        if x == y:
            b[y][x] = '*'
        elif x == (a-1)/2:
            b[y][x] = '*'
        elif x + y == 6:
            b[y][x] = '*'
        elif y == (a-1)/2:
            b[y][x] = '*'
for line in b:
    print(*line)