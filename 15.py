# Autor_ Marcelo Merchan
# Email__angel.merchan@unl.edu.ec

# Read a 2D list of integers:
# a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
# Print a value:
# print(a)

a,b = [int(s) for s in input().split()]
for i in range(0,a):
    lista = list(map(int, input().split()))
    if i == 0:
        num_max = max(lista)
        max_lis = [0,lista.index(max(lista))]
    elif max(lista) > num_max:
        num_max = max(lista)
        max_lis = [i,lista.index(max(lista))]
print(*max_lis, sep =' ')