# Autor_ Marcelo Merchan
# Email__angel.merchan@unl.edu.ec

# Given a list of numbers with all elements sorted in ascending order, determine and print the number
# of distinct elements in it.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

numeros = input()
lista_t = list(map(int, numeros.split()))
cont = 1

for n in range(1, len(lista_t)):
    if lista_t[n - 1] != lista_t[n]:
        cont += 1

print(cont)