# Autor_ Marcelo Merchan
# Email__angel.merchan@unl.edu.ec

# Instruccions__Given an integer n, create a two-dimensional array of size n×n according to the following rules and print it:

# Read an integer:
# a = int(input())
# Print a value:
# print(a)

y = int(input())
a = [[0] * y for i in range(y)]
for i in range(y):
  for j in range(y):
    if i + j + 1 < y:
      a[i][j] = 0
    elif i + j + 1 == y:
      a[i][j] = 1
    else:
      a[i][j] = 2
for linea in a:
  print(*linea)